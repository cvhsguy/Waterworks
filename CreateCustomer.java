import java.util.Scanner;
public class CreateCustomer {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int customerID;
        int customerCategory;
        String customerName;
        int customerZipcode;
        System.out.println("Welcome to the Uptown Waterworks website, here you can create" +
                " a new account.");
        System.out.println("First off, please put in your assigned ID and name.");
        customerID = in.nextInt();
        customerName = in.next();
        System.out.println("Next we will need your category, " +
                "1 for private, 2 for industrial and 3 for agricultural. and your zipcode");
        customerCategory = in.nextInt();
        customerZipcode = in.nextInt();
        CreateNewCustomer(customerID,customerCategory,customerName,customerZipcode);

        do {
            String data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                System.out.print(data);
            }
        } while (true);

    }
    public static void CreateNewCustomer(int customerID, int customerCategory,String customerName,int customerZipcode){
        DB.insertSQL("insert into tblCustomer values('"+customerID+"','"+customerName+"','"+customerCategory+"','"+customerZipcode+"');");
    }
}
