import java.util.Scanner;
public class PH {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input;
        String data = "";
        String test = "";
        var incrementor = 0;
        boolean loopbreaker = false;
        while (!loopbreaker) {
            System.out.print("What type of customer is this? Pick 1 for Private, 2 for Industrial, 3 for Agricultural" + "\n");
            input = in.nextInt();
            System.out.println(Category(input));
            if (input == 1 || input == 2 || input == 3) {
                loopbreaker = true;
            }
        }
        //This while loop above ensures that the input matches the expected input, otherwise it will keep looping.
        System.out.println("What is the customers ID?");
        int thing = in.nextInt();
        System.out.println("This is the total amount of water consumed the last half year: ");
        waterConsumed(thing);
        /*The waterConsumed is a method down below that contains a DB.selectsql function
        that adds up all the latest readings from watermeters based on the given customer ID.
        */
        do {
             data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                System.out.print(data);
            }
        } while (true);
        //This do while loop just displays the selected data.

        System.out.println("These are the taxes owed based on the water consumption and your customer category: ");
        Taxes(thing);
        /*This method basically does the same thing as the one mentioned above, although
        it is more complex in that it joins together two tables and multiplies the taxrate with the readings
        to get the taxes owed.
        */
        //taxrate is assumed to be 5% for private, 1% for industrial, 0.5% for agricultural
        //the  water price is assumed to be 1kr per 1l for convenience.
        do {
             data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                System.out.print(data);
            }
        } while (true);

        System.out.println("The total payment owed is: ");
       totalPayment(thing);
       /*This method works very similarly as to the one above in Taxes, it just adds the readings to the taxes
       to get the total amount owed, this works because we assume that the price per litre of water is 1kr,
       so the water amount can be easily to a monetary value.
       */
        //this query calculates the taxes by taking the (sum*tax)+sum, to get the total amount owed.
        do {
             data = DB.getDisplayData();
            if (data.equals(DB.NOMOREDATA)) {
                break;
            } else {
                System.out.print(data);
            }
        } while (true);
        System.out.println("Press 1 to generate the bill");
        int thing2 = in.nextInt();
        System.out.println("Uptown Waterworks Incorporated Settlement Bill");
        waterConsumed(thing);
        System.out.print("Water consumption: "+DB.getDisplayData());
        singularTax(thing);
        System.out.print("Water Tax: "+DB.getDisplayData());
        singularTax(thing);
        System.out.print("Drainage Tax: "+DB.getDisplayData());
        totalPayment(thing);
        System.out.print("Total amount owed: "+DB.getDisplayData());
        /*The prints + method calls above works the same as the ones shown earlier,
        but it is condensed to be in a "bill" form.
        */
        while (loopbreaker) {
            System.out.print("Has payment been received? yes / no");
            test = in.next();
            if(test.equals("yes")) {
                loopbreaker = false;
                System.out.println("Payment received, program complete.");
            } else if(test.equals("no")){
                incrementor++;
                System.out.println("A reminder has been sent, this is reminder #"+incrementor);
                if(incrementor>=4){
                    System.out.println("Customer #"+thing+"s water will be shut off.");
                    break;
                }
            }
        }
        /* This last while loop handles the last part of our module diagram of Payment Handling,
        the invoice has already been "sent" and now the waterworks administration is awaiting payment
        the user is given a choice to pick whether a bill has been paid or not by typing yes or no.
        If the input is no, then an incrementor is used to show the amount of reminders sent,
        and if the amount of reminders reaches 4, the water to the customer is shut off.
        */

    }

    public static String Category(int input) {
        if (input == 1) {
            return "private customer";
        } else if (input == 2) {
            return "Industrial customer";
        } else if (input == 3) {
            return "Agricultural customer";
        }
        return "";
        /*This if statement assigns a customer category based on the input value, so 1 is a private customer etc.*/
    }
    public static void waterConsumed(int input){
        DB.selectSQL("select sum(LatestReading) from tblReading where customerID=" + "'" + input + "'" + ";");
        /*This function gets the sum of the column LatestReading from tblReading
        The aim of this is to fetch the total water consumed to be used in further calculations.*/
    }
    public static void singularTax(int input){
        DB.selectSQL("select\n" +
                "  sum(dbo.tblReading.LatestReading * dbo.tblTax.taxRate)\n" +
                "from dbo.tblCustomer\n" +
                "join dbo.tblReading\n" +
                "  on dbo.tblCustomer.customerID = dbo.tblReading.customerID\n" +
                "join dbo.tblTax\n" +
                "  on dbo.tblCustomer.customerID = dbo.tblTax.customerID\n" +
                "where dbo.tblCustomer.customerID = " + input);
        /*This query joins three tables together, tblCustomer, tblReading and tblTax in order to get the relevant data
        to calculate the water/drainage tax which is the sum of LatestReading * taxRate.*/
    }
    public static void Taxes(int input){
        DB.selectSQL("select\n" +
                "  sum(dbo.tblReading.LatestReading * dbo.tblTax.taxRate)*2\n" +
                "from dbo.tblCustomer\n" +
                "join dbo.tblReading\n" +
                "  on dbo.tblCustomer.customerID = dbo.tblReading.customerID\n" +
                "join dbo.tblTax\n" +
                "  on dbo.tblCustomer.customerID = dbo.tblTax.customerID\n" +
                "where dbo.tblCustomer.customerID = " + input);
        /*This query does the same as the one above but adds water and drainage tax together by multiplying by 2.*/
    }
    public static void totalPayment (int input){
        DB.selectSQL("select\n" +
                "  sum(dbo.tblReading.LatestReading+(dbo.tblReading.latestReading * dbo.tblTax.taxRate)*2)\n" +
                "from dbo.tblCustomer\n" +
                "join dbo.tblReading\n" +
                "  on dbo.tblCustomer.customerID = dbo.tblReading.customerID\n" +
                "join dbo.tblTax\n" +
                "  on dbo.tblCustomer.customerID = dbo.tblTax.customerID\n" +
                "where dbo.tblCustomer.customerID = "+input);
        /*This last query adds the two taxes together and adds the water amount to that number to get the total amount owed.*/
    }

    }


    /*
    random notes
if first input = 1 tax rate = 0.05
if first input = 2 taxrate = 0.01
if first input = 3 taxrate = 0.005


store first input
assign taxrate according to first input
savevalue is the customer category

ctrl alt shift S to open libraries, find the jar file
     */